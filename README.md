# Salt master

_Configuration for a salt master that runs in a docker container, and states for
my cloud VMs._

## Contents of the repo:

### Boilerplate/utils

`cloud-init`: Init scripts for provisioning VMs ready to be managed by this salt
master. This are to be copy/pasted into the hosting provider's GUI when creating
machines.

`docker`: Files to build a docker image containing a salt master

`docker-compose`, `run.sh`: Docker-compose file to mount a bunch of files from
this repo in the salt-master container, and a script to start an interactive
shell in that container.

### Salt config files

`master`: Configuration file for the master

`roster`: List of minions for salt-ssh

`pillar`, `salt`: Salt states and pillar files.

`ssh`: The salt master's key-pair (which is pre-seeded on minions by the cloud
init script). Note, private keys in this repo are encrypted with `git-crypt`.

## Usage:

    ./run.sh
    ...
    [master] salt-ssh '*' test.ping
    ...
    (wait while the master pushes thin client files)
    ...
    1.2.3.4:
        True
    2.3.4.5:
        True
