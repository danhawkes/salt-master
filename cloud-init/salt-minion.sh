#!/bin/sh
set -e

mkdir -p /root/.ssh
chmod 600 /root/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQdFD8Y/mGv0VonpysPzprP9J3Aai80sTHZp5/sP0GPwEU7wSqr9d2++gf/gw/2BnHYReIlgs26vxfSVLdBcC1aOxHmX6L6hl2pI3w6p7xZY7xc4kzftsjA9jCR3gnXcs++bJUgugESreDLBQr/gd0J5nWNkCOHct6O2xbd1bofISijbV8XXyw2XqpNdEcTNl8VU9GCLV3E59oQWM/uosm5uAdEJLPWSIiW7D3gSwPJ54o2w2nbj+q6ZjKAF/bCpn8rR/0EClbX/8eoRZRltQoz/RRlFJmW61fQl+Y/nRF71keIXahF+gbMIraYVseQ0/h4ofshHuKknaU4VnSRAcz root@salt-master' >> /root/.ssh/authorized_keys
chmod 700 /root/.ssh/authorized_keys

retry_delay=10
for i in $(seq 1 60); do
  apt-get update && apt-get install -y python2.7 && break \
  || echo "Attempt ${i} failed, apt busy. Retrying in ${retry_delay}..."; sleep "$retry_delay"
done
