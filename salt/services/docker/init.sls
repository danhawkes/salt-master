docker_installed:
  pkg.installed:
    - name: docker.io
    - version: '17.12.1*'

docker_service_running:
  service.running:
    - name: docker
    - enable: True

docker_python_installed:
    pkg.installed:
      - name: python-docker
