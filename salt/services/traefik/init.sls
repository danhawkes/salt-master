include:
  - services/docker

{% set image_name = 'traefik:1.6.5-alpine' %}

traefik_root:
  file.directory:
    - name: /opt/traefik

traefik_data:
  file.directory:
    - name: /opt/traefik/data

traefik_config:
  file.managed:
    - name: /opt/traefik/traefik.toml
    - source: salt://services/traefik/traefik.toml
    - template: jinja

traefik_image:
  docker_image.present:
    - name: {{ image_name }}

traefik_network:
  docker_network.present:
    - name: traefik

traefik_container:
  docker_container.running:
    - name: traefik
    - image: {{ image_name }}
    - port_bindings:
      - '80:80'
      - '443:443'
      - '127.0.0.1:8080:8080'
    - binds:
      - /var/run/docker.sock:/var/run/docker.sock
      - /opt/traefik/traefik.toml:/etc/traefik/traefik.toml:ro
      - /opt/traefik/data:/opt/traefik
    - network_mode: traefik
