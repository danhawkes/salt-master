
nginx_repo:
  pkgrepo.managed:
    - ppa: nginx/stable

nginx_installed:
  pkg.installed:
    - name: nginx
    - version: '1.14.0-*'

nginx_cache_config:
  file.managed:
    - name: /etc/nginx/conf.d/cache.conf
    - source: salt://services/nginx/cache.conf
    - user: root
    - group: root
    - mode: 644

nginx_service_running:
  service.running:
    - name: nginx
    - enable: True
    - watch:
      - file: nginx_cache_config
