ssh_keys:
  ssh_auth.present:
    - names:
      - {{ pillar['ssh']['laptop'] }}
      - {{ pillar['ssh']['gitlab-ci'] }}
    - user: root

sshd_config:
  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://core/ssh/sshd_config
    - user: root
    - group: root
    - mode: 644

sshd_service:
  service.running:
    - name: sshd
    - enable: True
    - watch:
      - file: sshd_config
