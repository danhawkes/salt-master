# From https://serverfault.com/questions/628531/how-to-enable-swap-with-salt-stack

{% set size = grains["mem_total"] * 2 %}
{% set path = '/var/swapfile' %}

swapfile_present:
  cmd.run:
    - name: |
        swapon --show=NAME --noheadings | grep -q "^{{ path }}$" && swapoff {{ path }}
        rm -f {{ path }}
        fallocate -l {{ size }}M {{ path }}
        chmod 0600 {{ path }}
        mkswap {{ path }}
    - unless: bash -c '[[ $(($(stat -c %s {{ path }}) / 1024**2)) = {{ size }} ]]'

swap_mounted:
  mount.swap:
    - name: {{ path }}
    - persist: True