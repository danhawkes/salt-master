base_unattended_upgrades:
  pkg.installed:
    - name: unattended-upgrades

base_python_apt:
  pkg.installed:
    - name: python-apt

base_debconf_utils:
  pkg.installed:
    - name: debconf-utils

base_utils:
  pkg.installed:
    - names:
      - htop
      - ncdu
      - tree

