firewall_installed:
  pkg.installed:
    - name: firewalld

firewall_running:
  service.running:
    - name: firewalld
    - enable: True

{% set apps = salt['pillar.get']('apps', []) %}
{% set dokku = 'dokku' in apps %}
{% set nextcloud = 'nextcloud' in apps %}
{% set radicale = 'radicale' in apps %}
{% set syncthing = 'syncthing' in apps %}
{% set tor_relay = 'tor-relay' in apps %}

{% if dokku or nextcloud or radicale %}
firewall_service_http:
  firewalld.service:
    - name: http
    - ports:
      - 80/tcp
      - 443/tcp

{% endif %}

{% if tor_relay %}
firewall_service_tor:
  firewalld.service:
    - name: tor
    - ports:
      - 9001/tcp
      - 9001/udp
{% endif %}

{% if syncthing %}
firewall_service_syncthing:
  firewalld.service:
    - name: syncthing
    - ports:
      - 22000/tcp
      - 21027/udp
{% endif %}

firewall_zone_public:
  firewalld.present:
    - name: public
    - services:
      - ssh
      - dhcpv6-client
      {% if dokku or nextcloud or radicale %}
      - http
      {% endif %}
      {% if tor_relay %}
      - tor
      {% endif %}
      {% if syncthing %}
      - syncthing
      {% endif %}
