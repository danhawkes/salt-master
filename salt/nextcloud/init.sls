include:
  - services/docker
  - services/traefik

{% set image_name = 'nextcloud:13' %}

nextcloud_image:
  docker_image.present:
    - name: {{ image_name }}

nextcloud_container:
  docker_container.running:
    - name: nextcloud
    - image: {{ image_name }}
    - labels:
      - traefik.enable=true
      - traefik.frontend.rule=Host:nextcloud.danhawkes.co.uk
    - network_mode: traefik
