{% for name, config in pillar['cloudflare']['zones'].items() %}
cloudflare_zone_{{name}}:
  cloudflare.manage_zone_records:
    - name: {{ name }}
    - zone:
        auth_email: {{ pillar['cloudflare']['api_email'] }}
        auth_key: {{ pillar['cloudflare']['api_key'] }}
        zone_id: {{ config['zone_id'] }}
        records: {{ config['records'] | yaml }}
{% endfor %}
