include:
  - services/docker
  - services/nginx

dokku_repo:
  pkgrepo.managed:
    - name: deb https://packagecloud.io/dokku/dokku/ubuntu/ {{ pillar['ubuntu_codename'] }} main
    - file: /etc/apt/sources.list.d/dokku.list
    - key_url: https://packagecloud.io/gpg.key

# This key's referenced in the debconf. It's added as an authorised key for the dokku user.
dokku_ssh:
  file.managed:
    - name: /root/.ssh/laptop.pub
    - contents: {{ pillar['ssh']['laptop'] }}

dokku_debconf:
  debconf.set_file:
    - name: dokku
    - source: salt://apps/dokku/debconf.answers
    - template: jinja

dokku_installed:
  pkg.installed:
    - name: dokku
    - version: '0.11.2'
    - require:
      - docker_installed

dokku_plugin_letsencrypt:
  cmd.run:
    - name: dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
    - unless: dokku plugin:list | grep letsencrypt

{% set letsencrypt_email = pillar['letsencrypt']['email'] %}
dokku_config_letsencrypt:
 cmd.run:
   - name: dokku config:set --global --no-restart DOKKU_LETSENCRYPT_EMAIL={{ letsencrypt_email }}
   - unless: dokku config:get --global DOKKU_LETSENCRYPT_EMAIL | grep {{ letsencrypt_email }}

dokku_cron_letsencrypt:
 cron.present:
   - name: /var/lib/dokku/plugins/available/letsencrypt/cron-job
   - user: dokku
   - special: '@daily'

dokku_plugin_httpauth:
  cmd.run:
    - name: dokku plugin:install https://github.com/dokku/dokku-http-auth.git
    - unless: dokku plugin:list | grep http-auth
