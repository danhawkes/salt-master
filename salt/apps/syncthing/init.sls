syncthing_group:
  group.present:
    - name: syncthing

synthing_user:
  user.present:
    - name: syncthing
    - groups:
      - syncthing
    - shell: /usr/sbin/nologin

syncthing_repo:
  pkgrepo.managed:
    - name: deb https://apt.syncthing.net/ syncthing stable
    - file: /etc/apt/sources.list.d/syncthing.list
    - key_url: https://syncthing.net/release-key.txt

syncthing_installed:
  pkg.installed:
    - name: syncthing

syncthing_config:
  file.recurse:
    - name: /home/syncthing/.config/syncthing
    - source: salt://apps/syncthing/config
    - user: syncthing
    - group: syncthing
    - file_mode: 600
    - dir_mode: 700

syncthing_service:
  service.running:
    - name: syncthing@syncthing
    - enable: True
    - watch:
      - file: syncthing_config
