tor_repo:
  pkgrepo.managed:
    - name: deb http://deb.torproject.org/torproject.org/ {{ pillar['ubuntu_codename'] }} main
    - file: /etc/apt/sources.list.d/tor.list
    - keyid: 886DDD89
    - keyserver: keys.gnupg.net

tor_installed:
  pkg.latest:
    - name: tor

tor_torrc:
  file.managed:
    - name: /etc/tor/torrc
    - source: salt://apps/tor-relay/torrc
    - user: root
    - group: root
    - mode: 644

tor_service:
  service.running:
    - name: tor
    - enable: True
    - watch:
      - tor_torrc
