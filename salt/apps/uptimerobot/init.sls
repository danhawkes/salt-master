{% set api_key = pillar['uptimerobot']['api_key'] %}

uptimerobot_alert_contacts:
  uptimerobot.alert_contacts_present:
    - api_key: {{ api_key }}
    - alert_contacts:
      - friendly_name: primary contact
        type: 2
        value: uptimerobot-alerts@danhawkes.co.uk

uptimerobot_monitors:
  uptimerobot.monitors_present:
    - api_key: {{ api_key }}
    - monitors:
      - friendly_name: personal site - blog
        url: https://danhawkes.co.uk
        type: 2
        keyword_type: 2
        keyword_value: 'docker'
        alert_contacts:
          - primary contact
      - friendly_name: personal site - about
        url: https://danhawkes.co.uk/about
        type: 2
        keyword_type: 2
        keyword_value: 'contact'
        alert_contacts:
          - primary contact
      - friendly_name: personal site - software
        url: https://danhawkes.co.uk/software
        type: 2
        keyword_type: 2
        keyword_value: 'GitLab'
        alert_contacts:
          - primary contact
      - friendly_name: unblock tor
        url: https://unblock-tor.com
        type: 2
        keyword_type: 2
        keyword_value: 'Unblock'
        alert_contacts:
          - primary contact
