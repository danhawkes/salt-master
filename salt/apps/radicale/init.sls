include:
  - services/docker
  - services/traefik

{% set image_name = 'tomsquest/docker-radicale:2.1.8.4' %}

radicale_config:
  file.recurse:
    - name: /opt/radicale
    - source: salt://apps/radicale/config
    - user: root
    - group: root
    - file_mode: 644
    - dir_mode: 755
    - clean: True

radicale_data:
  file.directory:
    - name: /opt/radicale_data
    - user: root
    - group: root
    - dir_mode: 755
    # Don't recurse - files must be owned by container.

radicale_image:
  docker_image.present:
    - name: {{ image_name }}

radicale_container:
  docker_container.running:
    - name: radicale
    - image: {{ image_name }}
    - labels:
      - traefik.enable=true
      - traefik.frontend.rule=Host:dav.danhawkes.co.uk
    - binds:
      - /opt/radicale:/config:ro
      - /opt/radicale_data:/data
    - network_mode: traefik
    - watch:
      - file: radicale_config
