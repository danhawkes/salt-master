base:
  '*':
    - core/base
    - core/ssh
    - core/firewall
    - core/swap
  'I@apps:tor-relay':
    - apps/tor-relay
  'I@apps:syncthing':
    - apps/syncthing
  'I@apps:dokku':
    - apps/dokku
  'I@apps:external-config':
    - apps/uptimerobot
    - apps/cloudflare
  'I@apps:nextcloud':
    - apps/nextcloud
  'I@apps:radicale':
    - apps/radicale
